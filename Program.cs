﻿using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using M2Mqtt;

namespace Iotdotnetcorepublisher
{
    class Program
    {
        static void Main(string[] args)
        {

            string message = "Test message";
            string topic = "Hello/World";

            var iotEndpoint = "a2voxmy6jorank-ats.iot.us-east-1.amazonaws.com"; //<AWS-IoT-Endpoint>           
            var brokerPort = 8883;
            var clientId = Guid.NewGuid().ToString();
            var certPass = "Elutra123$";

            var certificatesPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "certs");

            var caCertPath = Path.Combine(certificatesPath, "RENT500001.cert.pem");
            var pfxPath = Path.Combine(certificatesPath, "rootCA.pfx");


            var caCert = X509Certificate.CreateFromCertFile(caCertPath);

            X509Certificate2 clientCert = new X509Certificate2(pfxPath, certPass);


            var client = new MqttClient(iotEndpoint, brokerPort, true, caCert, clientCert, MqttSslProtocols.TLSv1_2);

            client.Connect(clientId);
            Console.WriteLine($"Connected to AWS IoT with client id: {clientId}.");

            int i = 0;
            while (true)
            {
                client.Publish(topic, Encoding.UTF8.GetBytes($"{message} {i}"));
                Console.WriteLine($"Published: {message} {i}");
                i++;
                Thread.Sleep(5000);
            }
        }
    }
}
